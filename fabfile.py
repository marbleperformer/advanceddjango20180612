import os
from fabric.api import local 


ENVIRONMENT_NAME = 'venv'

REQUIRENMENTS_FILE_PATH = 'requirenments.pip'


def runserver(host='', port=''):
    local(
        f'''
        docker-compose exec server python manage.py runserver {host} {port}
        '''
    )


def startapp(name):
    local(
        f'''
        . {ENVIRONMENT_NAME}/bin/activate
        cd server
        python manage.py startapp {name}
        '''
    )


def makemigrations():
    local(
        f'''
        docker-compose exec server python manage.py makemigrations
        '''
    )

def makeemptymigration(appname):
    local(
        f'''
        docker-compose exec server python manage.py makemigrations {appname}        
        '''
    )

def migrate():
    local(
        f'''
        docker-compose exec server python manage.py migrate
        '''
    )


def createsuperuser():
    local(
        f'''
        docker-compose exec server python manage.py createsuperuser
        '''
    )


def shell():
    local(
        f'''
        docker-compose exec server python manage.py shell
        '''
    )


def createtestproducts(num=''):
    local(
        f'''
        docker-compose exec server python manage.py createtestproducts {num}
        '''
    )


def install(module_name=None, *args, **kwargs):
    if not module_name:
        local(
            f'''
            . {ENVIRONMENT_NAME}/bin/activate
            pip install -r {REQUIRENMENTS_FILE_PATH}
            '''
        )
    else:
        local(
            f'''
            . {ENVIRONMENT_NAME}/bin/activate
            pip install {module_name}
            '''
        )
        local(
            f'''
            . {ENVIRONMENT_NAME}/bin/activate
            pip freeze > server/{REQUIRENMENTS_FILE_PATH}
            '''
        )


def init():
    if not os.path.exists(ENVIRONMENT_NAME):
        local(
            f'''
            virtualenv {ENVIRONMENT_NAME}
            '''
        )
    
    else:
        local(
            f'''
            . {ENVIRONMENT_NAME}/bin/activate
            pip freeze > server/{REQUIRENMENTS_FILE_PATH}
            '''
        )


def bash(container):
    local(
        f'''
        docker-compose exec {container} bash
        '''
    )
