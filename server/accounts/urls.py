from django.urls import path

from .views import (
    account_login, account_signin
)


app_name = 'accounts'


urlpatterns = [
    path('signin/', account_signin, name='signin'),
    path('', account_login, name='login'),
]
