from django import forms
from .models import AccoutUser

from django.contrib.auth.forms import UserCreationForm


class AccountUserForm(forms.Form):
    username = forms.CharField(
        label='Login', max_length=150,
        widget=forms.widgets.TextInput(
            attrs={'class': 'field_username'}
        )
    )
    password = forms.CharField(
        max_length=250, 
        widget=forms.widgets.PasswordInput(
            attrs={'class': 'field_password'}
        )
    )


class BaseSigninForm(UserCreationForm):
    class Meta:
        model = AccoutUser
        fields = ['username', 'email', 'password1', 'password2']

    def save(self, commit=True):
        obj = super(BaseSigninForm, self).save(commit=False)
        password = self.cleaned_data.get('password')

        obj.set_password(password)
        obj.is_active = False

        if commit:
            obj.save()

        return obj


class AccountSigninForm(forms.ModelForm):
    password_confirm = forms.CharField(
        label='Confirm password',
        widget=forms.PasswordInput(),
        required=True,
    )

    class Meta:
        model = AccoutUser
        fields = ['username', 'email', 'password']
        
    def clean_password_confirm(self):
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')

        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Password is not confirm')

        return self.cleaned_data

    def save(self, commit=True):
        obj = super(AccountSigninForm, self).save(commit=False)
        password = self.cleaned_data.get('password')

        obj.set_password(password)
        obj.is_active = False        

        if commit:
            obj.save()

        return obj


class SigninForm(forms.Form):
    username = forms.CharField(
        label='Login', max_length=150,
        widget=forms.widgets.TextInput(
            attrs={'class': 'field_username'}
        )
    )
    email = forms.CharField(
        max_length=150, required=True,
        widget=forms.widgets.EmailInput(
            attrs={'class': 'field_email'}
        )
    )
    password = forms.CharField(
        max_length=250,
        widget=forms.widgets.PasswordInput(
            attrs={'class': 'field_password'}
        )
    )
    password_confirm = forms.CharField(
        max_length=250,
        widget=forms.widgets.PasswordInput(
            attrs={'class': 'field_password'}
        )
    )
