from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy
from django.core.mail import send_mail

from .forms import (
    AccountUserForm, AccountSigninForm
)


def account_login(request):
    success_url = reverse_lazy('main:index')
    form = AccountUserForm()

    if request.method == 'POST':
        form = AccountUserForm(data=request.POST)
        
        if form.is_valid():
            usr = form.cleaned_data.get('username')
            pwd = form.cleaned_data.get('password')
            user = authenticate(
                username=usr,
                password=pwd,
            )

            if user and user.is_active:
                login(request, user)
                
                return redirect(success_url)

    return render(request, 'accounts/login.html', {'form': form})


def account_signin(request):
    success_url = reverse_lazy('main:index')
    form = AccountSigninForm()

    if request.method == 'POST':
        form = AccountSigninForm(data=request.POST)
        
        if form.is_valid():
            user = form.save()
            login(request, user)

            send_mail(
                'Signin User',
                f'Test Message.',
                from_email='info@project.ru',
                recipient_list=[user.email],
            )

        return redirect(success_url)

    return render(request, 'accounts/signin.html', {'form': form})


@login_required(login_url=reverse_lazy('accounts:login'))
def confirm_account(request):
    success_url = reverse_lazy('main:index')
    
    request.user.is_active = True
    
    return redirect(success_url)
