"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf.urls.static import static
from django.conf import settings

from rest_framework.routers import DefaultRouter
from debug_toolbar import urls as debug_urls

from products.viewsets import (
    CategoryViewSet, ProductViewSet
)


router = DefaultRouter()
router.register('categories', CategoryViewSet, base_name='categories')
router.register('products', ProductViewSet, base_name='products')

django_router = [
    path('products/', include('products.routes')),
    path('categories/', include('products.routes.categories'))
]

urlpatterns = [
    path('api/', include((router.urls, 'rest'))),
    path('django_api/', include(django_router)),
    path('admin/', admin.site.urls),
    re_path(
        r'^auth/verify/google/oauth2/',
        include(
            'social_django.urls',
            namespace='social'
        ),
    ),
    path('products/', include('products.urls')),
    path('categories/', include('products.urls.categories')),
    path('accounts/', include('accounts.urls')),
    path('cart/', include('cart.urls')),
    path('', include('main.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += [path('__debug__/', include(debug_urls))]
