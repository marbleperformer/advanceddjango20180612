from django.db.transaction import atomic
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.forms import inlineformset_factory
from django.views.generic import (
    ListView, DetailView, CreateView, UpdateView
)

from .models import Purchase, PurchaseItem
from .forms import PurchaseItemForm


def cart_view(request):
    return render(request, 'cart/cart.html')


class PurchaseListView(ListView):
    model = Purchase
    template_name = 'cart/list.html'


class PurchaseCreateView(CreateView):
    model = Purchase
    fields = ['user', 'is_active']
    
    formset_model = PurchaseItem
    formset_fields = ['product', 'value']
    
    template_name = 'cart/create.html'
    success_url = reverse_lazy('cart:list')

    def get_formset_class(self):
        return inlineformset_factory(
            self.model,
            self.formset_model,
            fields=self.formset_fields,
        )

    def get_formset_kwargs(self):
        kwargs = {}

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })

        if hasattr(self, 'object'):
            kwargs.update(
                **{'instance': self.object}
            )

        return kwargs

    def get_formset(self):
        formset_class = self.get_formset_class()
        prefix = formset_class.get_default_prefix()
        
        formset_kwargs = self.get_formset_kwargs()
        formset_kwargs.update(
            **{'prefix': prefix}
        )

        return formset_class(
            **formset_kwargs
        )

    def get_context_data(self, **kwargs):
        context = super(PurchaseCreateView, self).get_context_data(**kwargs)
        
        context.update({
            'formset': self.get_formset()
        })

        return context

    def form_valid(self, form):
        form = self.get_form()

        with atomic():
            self.object = form.save()
            formset = self.get_formset()
            
            if formset.is_valid():
                formset.save()
                
                return redirect(self.success_url)

        return render(self.request, self.template_name)
