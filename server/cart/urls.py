from django.urls import path
from .views import (
    PurchaseListView, PurchaseCreateView, cart_view
)


app_name = 'cart'

urlpatterns = [
    path('create/', PurchaseCreateView.as_view(), name='create'),
    path('list/', PurchaseListView.as_view(), name='list'),
    path('', cart_view, name='cart'),
]
