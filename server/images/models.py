from datetime import datetime 
from django.db import models
from django.utils.functional import cached_property


class Image(models.Model):
    title = models.CharField(max_length=150)
    value = models.ImageField(upload_to='images')

    @property
    def url(self):
        return self.value.url

    @cached_property
    def get_top_new_products(self):
        return self.product_set.filter(
            created__lte=datetime.now()
        )[:10]

    def __str__(self):
        return self.title
        
